class Article < ApplicationRecord
  validates :title, :body, :released_at, presence: true
  validates :title, length: { maximum: 80 }
  validates :body, length: { minimum: 2000 }

  before_validation do
    self.expired_at = nil if @no_expiration
  end
  
end