module ApplicationHelper
    def page_title
        title = "News"
        title = @page_title + " - " + title if @page_title
        title
    end

    def menu_link_to(text, path, option = {})
        content_tag :li do
            link_to_unless_current(text, path, option) do
                content_tag(:span, text)
            end
        end
    end
end
